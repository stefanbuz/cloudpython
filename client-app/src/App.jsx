import React, { Component } from 'react';
import './App.css';
import PasteManager from './components/PasteManager'
import PasteCreator from './components/PasteCreator'
import PasteEditor from './components/PasteEditor'
import LocalStorage from './LocalStorage'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      screenState: 'PasteManager'
    }
    //Local storage loads and synchronizes local storage data to state.items propagating changes via setState
    this.localStorage = new LocalStorage(this);
    this.setScreenState = this.setScreenState.bind(this);
    this.handleScreenStateChange = this.handleScreenStateChange.bind(this);
  }
    

  setScreenState(newState){
    this.setState( {screenState:newState});
  }
  handleScreenStateChange(event){
    let newState = event.target.value;
    this.setScreenState(newState);
  }

  render() {
    var appComponents = {
      PasteEditor: (  
        <PasteEditor storage={this.localStorage} />
      ),
      PasteCreator:(
        <PasteCreator storage={this.localStorage} setScreenState={this.setScreenState} />
      ),
      PasteManager:(
        <PasteManager 
          storage={this.localStorage} 
          setScreenState={this.setScreenState} 
          localPasteIDs={this.localStorage.getItem('localPasteIDs',Array) }
        />
      )
    }
    var componentToRender = appComponents[this.state.screenState];
    return (
      <div className="App">
        <div className="App-header">
          <h1>Cloud Python</h1>
        </div>
        <div >
          <button onClick={this.handleScreenStateChange} value="PasteCreator">Create a new paste</button>
          <button onClick={this.handleScreenStateChange} value="PasteManager">View your pastes</button>
          {componentToRender}
        </div>    
      </div>
    );
  }
}

export default App;