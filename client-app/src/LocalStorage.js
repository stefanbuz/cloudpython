// local storage manages the storage api such that it syncs its state with 
class LocalStorage{
  constructor(reactComponent){
    reactComponent.state.items = {};
    for (let i = 0; i < window.localStorage.length; i++){
      let key = window.localStorage.key(i)
      reactComponent.state.items[key] = JSON.parse(window.localStorage.getItem(key));
    }
    
    this.reactComponent = reactComponent;
    this.getItem = this.getItem.bind(this);
    this.setItem = this.setItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
  }
  //desired type is used when no result is returned 
  //this makes it easier as you can avoid null checks on the result of getitem 
  //for operations such as map 
  //mutation of reactComponent's state via direct assignment
  getItem(key,desiredType){
    let item = this.reactComponent.state.items[key];
    //item is not in local storage and we were given a constructor we create an empty item
    if (item === undefined && typeof desiredType === 'function'){
      let emptyItem = new desiredType();
      item = emptyItem;
    }
    return item;
  }

  setItem(key,item){
    window.localStorage.setItem(key,JSON.stringify(item));
    this.reactComponent.setState( (prevState, props) => {
      prevState.items[key] = item;
      return { items: prevState.items }; 
    });
  }

  removeItem(key){
    window.localStorage.removeItem(key);
    this.reactComponent.setState( (prevState, props) => {
      delete prevState.items[key];
      return { items: prevState.items }; 
    });
  }
};

export default LocalStorage;
