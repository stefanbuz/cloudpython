import React, { Component } from 'react';
import Axios from 'axios';
import PropTypes from 'prop-types';
import Utilities from './Utilities.js'
import './css/PasteManager.css';

//PasteManager retrieves paste information from storage
//then displays contextual edit/delete actions for each paste ID
class PasteManager extends Component{
	constructor(props){
		super(props);
		this.langDisplayNames = {
			PYTHON: 'Python',
			PLAIN_TEXT: 'Plain Text'
		}
		this.deletePasteReq = this.deletePasteReq.bind(this);
		this.switchToEditor = this.switchToEditor.bind(this);		
	}

	deletePasteReq(event){
		let pasteID = event.target.name;	
		Axios( {
			method: 'delete',
			url: '/api/paste/'+pasteID
		})
		.then( ()=>{
			//Only delete the paste information from storage once the server
			//has confirmed it has deleted the paste 
			let pasteToDeletePos = this.props.localPasteIDs.indexOf(pasteID);
			this.props.localPasteIDs.splice(pasteToDeletePos,1);
			this.props.storage.removeItem(pasteID); 
			this.props.storage.setItem('localPasteIDs',this.props.localPasteIDs);	
		})
		.catch( (error)=>{
			Utilities.apiRequestErrHandler(error);
		});
	}

	switchToEditor(event){
		let pasteID = event.target.name;
		this.props.storage.setItem('activePaste',pasteID);
		this.props.setScreenState('PasteEditor');
	}	

	render(){
		let pasteListElements = this.props.localPasteIDs.map( (pasteID,context) => {
			let localPasteInfo = this.props.storage.getItem(pasteID);
			return ( 	
				<tr key={pasteID}>
					<td>{localPasteInfo.title}</td>
					<td>{this.langDisplayNames[localPasteInfo.language]}</td>
					<td> 
						<button name={pasteID} onClick={this.switchToEditor}>Edit Paste</button>
						<button name={pasteID} onClick={this.deletePasteReq}>Delete Paste</button>
					</td>
				</tr>
			)}
		); 		
		return(			
			<div>
				<h2>Pastes</h2>
				<table className="PasteTable" >
					<thead>		
						<tr>
							<th>Title</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>		
						{pasteListElements}
					</tbody>
				</table>
			</div>
		)
	}
}
PasteManager.propTypes = {
	storage: PropTypes.shape({
		getItem: PropTypes.func,
		setItem: PropTypes.func,
		removeItem: PropTypes.func
	}).isRequired,
};

export default PasteManager;