import React, { Component } from 'react';
import Axios from 'axios'
import PropTypes from 'prop-types';
import MessageBox from './MessageBox'
import SourceCodeInput from './SourceCodeInput'
import Utilities from './Utilities.js';
import './css/PasteEditor.css';

//PasteEditor loads the active paste from the cloud
//and offers the run and update remote operations upon the loaded paste
class PasteEditor extends Component{
  constructor(props){
    super(props);	
    this.langDisplayNames = {
			PYTHON: 'Python',
			PLAIN_TEXT: 'Plain Text'
		}
    this.pasteIDToLoad = this.props.storage.getItem('activePaste');
    this.state = {
      pasteActionStatus: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.updatePaste = this.updatePaste.bind(this);
    this.runPaste = this.runPaste.bind(this);
    this.mutatePasteText = this.mutatePasteText.bind(this);
  }
  
  componentDidMount(){
    if (this.pasteIDToLoad === undefined){
      return;
    }
    else{
      Axios({
        method: 'get',
        url: '/api/paste/' + this.pasteIDToLoad
      })
      .then( (response)=>{
        this.setState({
          currentPaste: response.data.paste,
        });
      })
      .catch( (error)=>{
        this.setState( {currentPaste:null} );
        Utilities.apiRequestErrHandler(error);
      });
    }
  }

  handleChange(event){
    var value = event.target.value;
    this.setState( (prevState, props) => {
      prevState.currentPaste.pasteText = value;
      return { currentPaste: prevState.currentPaste };
    });
  }

  updatePaste(){
    this.setState( {pasteActionStatus:'updating...'} );
    Axios({
      method: 'put',
      url: '/api/paste/' + this.state.currentPaste._id,
      data: this.state.currentPaste
    })
    .then( (response)=>{
      this.setState( {pasteActionStatus:'Paste Updated'} );
    })
    .catch( (error)=>{
      this.setState( {pasteActionStatus:'Paste update failed'} );
      Utilities.apiRequestErrHandler(error);
    });  
  }

  runPaste(){
    this.setState( {pasteActionStatus:'running...'} );
    Axios({
      method: 'post',
      url: '/api/paste/' + this.state.currentPaste._id,
    })
    .then( (response)=>{
      this.setState( {pasteActionStatus: response.data.output} );
    })
    .catch( (error)=>{
      this.setState( {pasteActionStatus:'Execution request could not be processed'} );
      Utilities.apiRequestErrHandler(error);
    });  
  }

  mutatePasteText(newPasteText){
    this.setState( (prevState)=>{
      prevState.currentPaste.pasteText = newPasteText;
      return { currentPaste: prevState.currentPaste }
    });
  }

  render(){
    if (this.state.currentPaste === null){
      return (
        <MessageBox message="Could not load paste from the cloud"/>
      );
    }
    else if (this.state.currentPaste === undefined){
      return ( <div></div> );
    }
    return (
      <div>
        <div>
          <h2> {this.langDisplayNames[this.state.currentPaste.language] + " Paste Title: " + this.state.currentPaste.title}</h2>
          <div className="Paste-text-area">
             <SourceCodeInput value={this.state.currentPaste.pasteText} updateModel={this.mutatePasteText} onChange={this.updatePaste}/> 
          </div>
        </div>        
        <button className="Paste-button" onClick={this.updatePaste}>
          Update Paste
        </button>
        <button className="Paste-button" onClick={this.runPaste}>
          Run Paste
        </button>
        <MessageBox message={this.state.pasteActionStatus} />
      </div>
    );
  } 
}

PasteEditor.propTypes = {
	storage: PropTypes.shape({
		getItem: PropTypes.func
	}).isRequired,
};

export default PasteEditor;