import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './css/MessageBox.css';

//MessageBox renders an element that displays a simple string message 
class MessageBox extends Component{
  render(){
    return (
      <span className="Message-Box">{this.props.message}</span>
    );
  }
}

MessageBox.propTypes = {
  message: PropTypes.string.isRequired,
};

export default MessageBox;