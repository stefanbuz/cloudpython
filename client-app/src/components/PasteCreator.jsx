import React, { Component } from 'react';
import './css/PasteCreator.css';
import Axios from 'axios'
import PropTypes from 'prop-types';
import SourceCodeInput from './SourceCodeInput'
import MessageBox from './MessageBox';
import Utilities from './Utilities.js';

//Pastecreator is a form style component for paste creation  
class PasteCreator extends Component{
	constructor(props){
		super(props);
		this.state = {
			pasteTitle: 'Title',
			pasteType: 'PYTHON',
			pasteText: '',
			status: ''
		};
		this.createPasteReq = this.createPasteReq.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.mutatePasteText = this.mutatePasteText.bind(this);
	};
	createPasteReq(){
		Axios( {
			method: 'post',
			url: '/api/paste/',
			data: {
				title: this.state.pasteTitle,
				language: this.state.pasteType,
				pasteText: this.state.pasteText
			}
		})
		.then( (response)=>{
			let createdPasteId = response.data.paste._id;
			let localStorage = this.props.storage;

			let localPastedIDs = localStorage.getItem('localPasteIDs', Array);
			localPastedIDs.push(createdPasteId);
			//Save the title and language for listing in PasteManager
 			let savedPasteInfo = {
				title: response.data.paste.title,
				language: response.data.paste.language
			}; //Note: saving the paste text as well could quickly overflow the limitations of browser local storage
			localStorage.setItem(createdPasteId, savedPasteInfo);
			localStorage.setItem('localPasteIDs', localPastedIDs);
			this.setState( {status:response.data.status} );
			setTimeout( ()=>{ this.props.setScreenState('PasteManager'); }, 2000); 
		})
		.catch( (error)=>{
			Utilities.apiRequestErrHandler(error,'Paste Creation',this);
		});
	}

	handleChange(event){		
		if (event.target.name in this.state ){
			let stateUpdate = {};
			stateUpdate[event.target.name] = event.target.value;
			this.setState( stateUpdate );
		}		
	}

	mutatePasteText(newValue){
		this.setState( {pasteText:newValue} );
	}

	render(){
		return (
			<div>
				<div className="PasteCreator-text-area">
					<SourceCodeInput value={this.state.pasteText} updateModel={this.mutatePasteText}/>
				</div>
				<MessageBox message={this.state.status}/>
				<div className="PasteCreator-controlDiv">
					<span>
						<label>Paste Type</label> 
						<select name="pasteType" value={this.state.pasteType} onChange={this.handleChange}>
							<option value="PYTHON">Python</option>
							<option value="PLAIN_TEXT">Plain Text</option>
						</select>
					</span>
					<span>
						<label>Paste Title</label>
						<input name="pasteTitle" value={this.state.pasteTitle} onChange={this.handleChange} type="text"  />
					</span>
					<button onClick={this.createPasteReq} className="Paste-button">Create Paste</button>
				</div>
			</div>			
		);
	}
}

PasteCreator.propTypes = {
	storage: PropTypes.shape({
		setItem: PropTypes.func
	}).isRequired,
	setScreenState: PropTypes.func.isRequired
};

export default PasteCreator;