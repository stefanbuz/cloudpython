import React, { Component } from 'react';
import PropTypes from 'prop-types';

/*
  SourceCodeInput boxes a text area element to improve its suitability as an input for source code
 
  Note: SourceCodeInput should be part of a larger component that contains a model
*/
class SourceCodeInput extends Component{
  constructor(props){
    super(props);
    this.state = {
      cursorPos: 0
    };
    this.handleTabs = this.handleTabs.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  /* 
  one undesirable issue with using a textarea for input is that the default browser behavior for 
  the Tab key is to iterate through clickable elements 

  as a solution, the handleTabs event handler for onKeyDown on a text input will insert 4 spaces 
  at the cursor for each tab down
  */
  handleTabs(event){
    if ( event.key === 'Tab'){
      event.preventDefault();
      let cursorPos = event.target.selectionStart;
      let prefix = this.props.value.slice(0,cursorPos)
      let fourSpaces = '    '; 
      let suffix = this.props.value.slice(cursorPos);
      this.props.updateModel(prefix + fourSpaces + suffix);
      this.setState( {cursorPos:cursorPos+4}, ()=>{
        //callback sets the cursor position after the component is mounted 
        this.textarea.selectionStart = this.state.cursorPos;
        this.textarea.selectionEnd = this.state.cursorPos;
      });
    }  
  }

  handleChange(event){
    this.props.updateModel(event.target.value);
    this.props.onChange(); //if a callback provided
  }

  render(){
    return(
      <textarea
        value={this.props.value}
        onChange={ this.handleChange }
        onKeyDown={this.handleTabs}
        ref={ (textarea)=>{ this.textarea = textarea } } >
      </textarea>
    );
  }
}

SourceCodeInput.propTypes = {
  value: PropTypes.string.isRequired,
  updateModel: PropTypes.func.isRequired,
  onChange: PropTypes.func
}

export default SourceCodeInput;