/**
 * reports the error message hierarchy formed by the Axios XHR library and the cloud python API for API requests 
 * @param {*} error 
 * @param {string} opname - name of the API operation that has failed
 * @param {component} [component] - optionally the handler can save the error message to a JSX component
 */
function apiRequestErrHandler (error,opname,component){
  let errorText = '';
  if (error.response){
    //The Cloud Python API on error will have a reason prop in its response
    if ( error.response.data.reason !== undefined){
      errorText =  opname + ' failed: ' + error.response.data.reason;
    } 
    //Error responses that don't conform to the Cloud Python API
    else {
      //Note: WebPack proxy requests that don't receive a proxied response will be caught here
      errorText = opname + ' failed: ' + error.response.data;
      console.log(error.response.data);
    }
  }
  else if (error.request){
    errorText = opname + ' failed: Cloud Python servers could not be reached';
    console.log(error.request);
  }
  else{
    errorText = opname + ' failed : Cloud Python could not make the request';
    console.log(error.message);
  }
  if (component !== undefined){
    component.setState( {status: errorText} );
  }
}

var Utilities = {}
Utilities.apiRequestErrHandler = apiRequestErrHandler;

export default Utilities;