/**
 * testClient.js dispatches python test programs from the tests subdirectory
 *  to be run on a pasteConsumer server via rabbitMQ
 *  USAGE: node testClient.js some_progam_in_tests_dir.py  
 */
//Note: testclient and pasteconsumer use the same rabbitconfig
const rabbitStatus = require('./config/rabbitConfig')();
const rabbit = require('rabbot');
const fs = require('fs');

rabbitStatus.once('connectionFailure', ()=>{
	console.log('Could not reach rabbitMQ');
	process.exit(1);
});

rabbitStatus.once('ready', ()=>{ 
    console.log('rabbit ready');
    if (process.argv[2] === undefined){
        console.log('no test python program specified.');
        console.log('USAGE: node testClient.js some_progam_in_tests_dir.py')
        process.exit(1);
    } 
    try {
        var testProgram = fs.readFileSync('./tests/'+process.argv[2], 'utf8');
    } catch (error) {
        console.log('Python test program '+process.argv[2]+' does not exist');
        process.exit(1)
    }
    
    var msgBody = {
        input: testProgram
    }
    var msg = {
        routingKey: "paste_queue",
        body: msgBody,
        contentType: "application/json"
    }
    //rabbot request publishes to a queue and then expects a response on a generated response queue
    rabbit.request('',msg)
    .then( (final) => {
        if (final.body.output.length > 2000)
            console.log('Output length:',final.body.output.length,'\n','Exit Code:',final.body.exitcode)
        else 
            console.log(final.body.output,'\n','Exit Code:',final.body.exitcode);
    })
    .catch( (err)=>{ console.log(err); } )
    .done( ()=>{ process.exit(0)} )

});