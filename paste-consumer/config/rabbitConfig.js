//all rabbitMQ toplogy is declared here
var settings = {
	connection: {
		user: "west",
		pass: "west",
		server: "192.168.1.14",
		replyTimeout: "70000"
	},
	queues:[
		{ name:"paste_queue", messageTtl:15000, durable:false, autoDelete:true, limit:50}
	]
}

module.exports = function(rabbitmqURI){
	var rabbit = require('rabbot');
	var events = require('events');

	if (rabbitmqURI !== undefined){
		settings.connection = {uri:rabbitmqURI};
	}
	var rabbitStatus = new events.EventEmitter();
	rabbit.configure(settings).done( 
		()=>{ rabbitStatus.emit('ready'); },
		()=>{ rabbitStatus.emit('connectionFailure');} );
	return rabbitStatus;
}