var pasteConsumer = require('./pasteConsumer');
var rabbitStatus = require('./config/rabbitConfig')();

rabbitStatus.once("connectionFailure", ()=>{
	console.log("Could not reach rabbitMQ");
	process.exit(1);
});
rabbitStatus.once("ready", ()=>{ 
    console.log('rabbit ready');
    pasteConsumer();
});
