const rabbit = require('rabbot');
const cp = require('child_process');

//runtime constraints on the docker container that pastes are executed within
var dockerOpts = [
	'run',
	'--rm', //delete the container once finished
	'-i', //keep STDIN open
	'--network', 'none', //Disable all incoming and outgoing networking in container
	'--cap-drop', 'ALL', 
	'--security-opt','seccomp:seccomp/profile.json', /* the system call seccomp whitelist policy
	 specified by profile.json is stricter than the default seccomp profile in that it does not whitelist socket related syscalls
	 , the policy could be further restricted without breaking docker functionality */
	//Resource constraints - https://docs.docker.com/engine/admin/resource_constraints/
	'--cpus', '0.100', /* cpus(hare) is a fraction of cpu quota over period 
	this is useful in preventing a computation bound program from becoming a resource hog*/
	'--memory', '250m', //total memory allocation maximum of 250 megabytes
	'--memory-swappiness', '0', //disable page swapping
	'stefanbuz/cloudpython:pyrunner' //the pyrunner docker image compiles and executes a program passed in via STDIN 
]
const PROGRAM_TIMEOUT_MS = 30000; 
const OUTPUT_STR_MAX_LENGTH = 1000000; /*limit on output from paste programs to prevent them from overflowing the paste consumer server*/

function truncateStr(str,limit){
	return str.substr(0,limit);
}

/**
 * pasteConsumer listens for pastes and executes them via a docker container
 * precondition: a rabbitMQ connection setup by the rabbot library
 * Protocol:
 *  pasteConsumer expects a JSON msg with an input prop (containing the paste program)
 *  the reply has an output prop and an exitcode prop
 */
module.exports = function(){
	var pasteHandlerOptions = {
		queue: 'paste_queue',
		handler: function(message){ 
			var pyrunner = cp.spawn('docker',dockerOpts);
			var output = 'OUTPUT:\n';
			pyrunner.on('close', (code) => {
				message.reply( {output: output, exitcode: code}  )
			}); 
			var timeout = setTimeout( () => {
				output += 'Program took too long to complete';
				pyrunner.kill('SIGKILL');
			}, PROGRAM_TIMEOUT_MS);
			pyrunner.stdout.on('data', (data) => {
				output += data.toString();
				output = truncateStr(output,OUTPUT_STR_MAX_LENGTH);
			});
			pyrunner.stderr.on('data', (data) => {
				output += data.toString();
				output = truncateStr(output,OUTPUT_STR_MAX_LENGTH);
			});
			//send the program to the docker image once all event handlers are registered
			pyrunner.stdin.write(message.body.input,'utf-8', () => {
				pyrunner.stdin.emit('end');
			}); 
		}        
	} 
	rabbit.handle(pasteHandlerOptions);
	rabbit.startSubscription('paste_queue')
}