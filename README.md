Cloud Python
============
Overview
------------
Cloud Python is a pastebin with the capability to execute saved pastes 

Cloud Python is composed of three interrelated applications. 

* paste consumer - a nodeJS server that listens for pastes to execute on a RabbitMQ connection

* app - an Express web app that provides the paste API and serves the client-app

* client-app - a ReactJS app that uses the local storage API of browsers to negate the need for user accounts

Development Setup
------------
A RabbitMQ and a MongoDB server must be present for the operation of paste-consumer and the app.
Both applications have config subdirectories where defaults are loaded from. These defaults 
can be overridden through URIs passed in through CLI arguments.

Note: The development client-app runs on a webpack server. For XHRs from the client-app to succeed,
the proxy property in the client-app's package.json must match the app server.

Production Setup
------------
A production setup differs in that the client app must be built. The following scripts assist. 

* node run build - builds and copies the production build to the public folder

* node run clean - cleans the public folder

Operation
------------
The app server will shutdown if it loses connection to RabbitMQ and/or MongoDB after
several attempts at reconnection. It is intended that this will lead to a load balancer such as HAproxy or Nginx
detecting this during a health check and taking the server out of circulation. 

Testing
------------
Testing tools are as follows:

* API endpoints - A Postman collection tests the CRUD and execute API of the app server

* paste consumer server - a python test client can submit test programs from its test directory to the paste consumer  

History
------------
CloudPython began life as a personal final project for a Fall 2015 cloud computing course where it was 
ultimately distributed amongst 40 machines managed by OpenNebula. 

It has been revisited in Spring 2017 to improve upon it after retrospection.  

Version 2.0.0 Changes

* The paste consumer server was migrated from Python to Node

* The paste execution mechanism of the paste consumer server was Dockerized

* Python paste execution via PyPy interpreter was removed 

(the PyPy interpreter sandboxing feature was discontinued)

* The AngularJS 1 based UI was replaced by a ReactJS app