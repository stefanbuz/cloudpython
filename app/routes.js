//route mappings and their callbacks
var controllers = {};
controllers.paste = require('../app/controllers/pasteController');
const rabbit  = require('rabbot');

module.exports = function (app,apiLimiter){
	//Paste API
	app.post('/api/paste/',apiLimiter, controllers.paste.create); 
	app.get('/api/paste/:pasteid', controllers.paste.read); 
	app.put('/api/paste/:pasteid', controllers.paste.update); 
	app.delete('/api/paste/:pasteid', controllers.paste.delete);
	app.post('/api/paste/:pasteid',apiLimiter, controllers.paste.executePaste);
}