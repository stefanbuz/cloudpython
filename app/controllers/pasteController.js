/**
 * pasteController.js provides CRUD operations for pastes and paste execution
 * 
 * Pastes are JSON objects with props for title, language and the pasteText itself
 * 
 * Protocol: each CRUD operation replies with a status prop reflecting the operation outcome,
 *  other properties are contextual to the operation and its outcome
 */
const paste = require('mongoose').model('paste');
const rabbit = require('rabbot');

//Generic failure response for the CRUD operations
function sendFailureResponse(res,err){
	var resObj = {
		status: 'FAILED',
		reason: err.message
	}
	res.status(404).json(resObj);
}

exports.create = function(req,res){
	var newPasteDoc = new paste( req.body);
	paste.create(newPasteDoc)
	.then( () => {
		var resObj = {
			status: 'CREATED',
			paste: newPasteDoc
		};
		res.status(200).json(resObj);
	})
	.catch( (err)=>{ sendFailureResponse(res,err); } );
}

exports.read = function(req,res){
	var searchid = req.params.pasteid;
	var query = paste.findById( searchid);
	query.exec()
	.then( (queryResultDoc)=>{
		if (queryResultDoc === null){
			res.status(404).json( {status:'NOT FOUND'} );
		}
		else {
			var resObj = {
				status: 'READ',
				paste: queryResultDoc
			};
			res.status(200).json(resObj);
		}
	})
	.catch( (err)=>{ sendFailureResponse(res,err); } );
}

exports.update = function(req,res){
	var updateid = req.params.pasteid;
	var searchQuery = paste.findById( updateid);
	searchQuery.exec()
	.then( (queryResultDoc)=>{
		if (queryResultDoc === null){
			res.status(404).json( {status:'NOT FOUND'} );
		}
		else {
			//Schema constructor wont allow mutation of the _id in any case, 
			//but why trouble the validators
			delete req.body._id;
			delete req.body.__v;
			for (var prop in req.body){
				queryResultDoc[prop] = req.body[prop];
			}
			queryResultDoc.save().then( ()=>{
				res.status(200).json( {status:'UPDATED'} );
			})
			.catch( (res)=>{ sendFailureResponse(res,err); });
		}
	})
	.catch( (err)=>{ sendFailureResponse(res,err); } );
}

exports.delete = function(req,res){
	var deletionid = req.params.pasteid;
	var deletionQuery = paste.findByIdAndRemove(deletionid);
	deletionQuery
	.then( ()=>{
		res.status(200).json( {status:'DELETED'} );
	})
	.catch( (err)=>{
		sendFailureResponse(res,err);
	});
}
/**
 * Loads a saved paste and executes it on a paste consumer server via rabbitmq
 * 
 * Precondition: a rabbitMQ connection setup by the rabbot library 
 * NOTE: it may appear that there is a resource leak with generated queues but this is simply because rabbot
 *  chooses to defer cleanup to batchs for performance reasons
 */
exports.executePaste = function(req,res){
	var executeID = req.params.pasteid;
	var query = paste.findById(executeID);
	query.exec()
	.then( (queryResultDoc) => {
		if (queryResultDoc === null){
			res.status(404).json( {status:'NOT FOUND'} );
		}
		else {
			var pasteToExec = queryResultDoc;
			if (pasteToExec.language === 'PLAIN_TEXT'){
				res.status(200).json( {status:'EXECUTED',output:'Plain text is not runnable'} );
			}
			var msgToPasteConsumer = {
				routingKey: "paste_queue",
				body: {
					input: pasteToExec.pasteText
					//a language prop here could be used to support additional languages
				},
				contentType: "application/json"
			};
			rabbit.request('',msgToPasteConsumer)
			.then( (final) => {
				res.status(200).json( {status:'EXECUTED',output: final.body.output} );
			})
			.catch( (err)=>{
				sendFailureResponse(res,err);
			});
		}  
	})   
	.catch( (err)=>{
		sendFailureResponse(res,err);
	});
}