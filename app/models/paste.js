var mongoose = require('mongoose');
uuidV4 = require('uuid/v4');

var supportedLanguages = { 
	PLAIN_TEXT: 1,
	PYTHON: 2
}
function languageValidator(val){
	//return true if the property is defined
	return val in supportedLanguages;
}

var pasteSchema = new mongoose.Schema(
	{
		title: {
			type: String,
			required: true,
			maxlength: 64,
		},
		language: {
			type: String, 
			required: true,
			uppercase: true,
			validate: languageValidator,
		 },
		pasteText: {
			type: String,
			required: true,
			maxlength: 32768, 
		},
		_id: {
			type: String,
			default: uuidV4, //generate a random v4 UUID 
			set: uuidV4 //ignore _id if passed to document constructor
		}
	},
	{
		safe: true,
	}
);
mongoose.model('paste',pasteSchema);