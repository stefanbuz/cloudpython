var settings = {
	port: 9005
}

const express = require('express');
const bodyParser = require('body-parser');
const RateLimit = require('express-rate-limit');

module.exports = function(port){
	if (port === undefined){
		port = settings.port;
	}
	var app = express();
	app.listen(port);
	console.log('Listening on port: ' + port);

	//Various configurations of express and middleware usages
	app.use(bodyParser.json() );
	//client app  production builds created via webpack are deployed to the public folder
	app.use(express.static('public'));
	//use the express-rate-limit middleware to protect availability as the app api is fully public
	app.enable('trust proxy'); 
	var apiLimiter = new RateLimit({
		windowMs: 5*60*1000, // quota expiration period in ms  (5 minutes)
		max: 50,
		delayMs: 0, // disabled
		handler: function (req, res){
			var resObj = {
				status: 'FAILED',
				reason: 'Quota exceeded'
			}
			res.status(404).json(resObj);
		}
	});
 	//app.use('/api/', apiLimiter);
		
	require('../app/routes')(app,apiLimiter);
	//top level error handler 
	app.use( (err, req, res, next)=>{
		var resObj = {
			status: 'FAILED',
			reason: 'Request could not be handled'
		};
		res.status(404).send( JSON.stringify(resObj));
	}); 

	return app;
};