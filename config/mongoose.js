var settings = {
	host:'192.168.1.12'
}

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

module.exports = function(mongoURI){
    if ( mongoURI === undefined)
	    mongoURI = 'mongodb://'+settings.host+'/cloudPython';
	var db = mongoose.connect(mongoURI);

	var connection = mongoose.connection;
	connection.on('error', console.error.bind(console, 'connection error:'));
	connection.once('open', ()=> {
		console.log('db connected');
	});

	//Load the schema/models
	require('../app/models/paste');

	return db;
};