//all rabbitMQ toplogy is declared here
var settings = {
	connection: {
		user: "west",
		pass: "west",
		server: "192.168.1.14",
		replyTimeout: "70000"
	},
	queues:[
		{ name:"rpcQueue", messageTtl:10000, durable:false, autoDelete:true, limit:8}
	]
}

var rabbit = require('rabbot');
var events = require('events');

module.exports = function(rabbitmqURI){
	if (rabbitmqURI !== undefined){
		settings.connection = {uri:rabbitmqURI};
	}
	var rabbitStatus = new events.EventEmitter();
	rabbit.configure(settings).done( 
		()=>{ rabbitStatus.emit("ready"); },
		()=>{ rabbitStatus.emit('connectionFailure');} );
	return rabbitStatus;
}