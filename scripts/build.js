const shell = require('shelljs');

shell.cd('client-app');
shell.exec('npm run build');
shell.cd('..');
shell.mv('client-app/build/*','public/')