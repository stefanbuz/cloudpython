//Optional CLI based configuration will override configuration set in ./config
var mod_getopt = require("posix-getopt");
var parser, option;
parser = new mod_getopt.BasicParser(':ha:(appPort)m:(mongoURI)r:(rabbitmqURI)',process.argv);
while ((option = parser.getopt()) !== undefined) {
	switch (option.option) {
	case 'h':
		console.log("Usage\n -a, --appPort\n -m, --mongoURI\n -r, --rabbitmqURI");
		process.exit(0);
		break;
    case 'a':
        var appPort = option.optarg;
        break;
    case 'm':
        var mongoURI = option.optarg;
        break;
    case 'r':
        var rabbitmqURI = option.optarg;
        break;
    default:
        console.log("unrecognized option");
        console.log("Usage\n -a, --appPort\n -m, --mongoURI\n -r, --rabbitmqURI");
        process.exit(0);
        break;
    }
}   

//Startup MongoDB 
var mongoose = require('./config/mongoose');
var db = mongoose(mongoURI);

//Startup RabbitMQ
var rabbitStatus = require('./config/rabbitmq')(rabbitmqURI); 
rabbitStatus.once("ready", ()=>{ console.log('rabbit ready');} );
rabbitStatus.once("connectionFailure", ()=>{
	console.log("Could not reach rabbitMQ");
    process.exit(1);
});

//Startup Express
var express = require('./config/express');
var app = express(appPort);

process.on('SIGINT', () => {
	console.log('Received SIGINT. Cleaning up and shutting down');
	process.exit(0);
});